export const ArticleType = Object.freeze({
  Post: 'post',
  Note: 'note',
  Page: 'page',
} as const)
