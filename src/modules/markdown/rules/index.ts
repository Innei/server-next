import { mentions } from './mentions'
import { spoiler } from './spoiler'

export const rules = { mentions, spoiler }

export default Object.values(rules)
