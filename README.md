# MX server next generation

Quick usage

Requirement:
  - Node.js 16+

If you're using Ubuntu you can just download the latest [release](https://github.com/mx-space/server-next/releases/latest) and unzip it.

```
node index.js
```

---

[![wakatime](https://wakatime.com/badge/github/mx-space/server-next.svg)](https://wakatime.com/badge/github/mx-space/server-next)

---

Interceptor Dataflow

```
ResponseInterceptor -> JSONSerializeInterceptor -> CountingInterceptor -> HttpCacheInterceptor
```

---

2021-08-31

开始写了

---

NestJS 8 发布了, 打算找个时间重构一下现有的后端.

但是最近在实习, 真的没有时间啊, 烦.

实习不太顺利, 门槛好高.

这个项目先立个项吧.
